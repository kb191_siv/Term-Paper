﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows;


namespace Calculator
{
    public partial class Calculator : Form
    {
        double a;
        double b;
        double rezultat;
        string znak;

        public Calculator()
        {
            InitializeComponent();
 
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_Plus_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = "";
            txbx_history.Text = this.txbx_pole1.Text + "+";
            try
            {
                znak = "+";
                if (a == 0)
                {
                    a = Convert.ToDouble(txbx_pole1.Text);
                }
                else
                {
                    b = Convert.ToDouble(txbx_pole1.Text);
                }                
                this.txbx_pole1.Text = "";
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_zapyata_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.txbx_pole1.Text = txbx_pole1.Text + ",";
        }

        private void btn_Minus_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = this.txbx_pole1.Text + "-";
            try
            {
                znak = "-";
                if (a == 0)
                {
                    a = Convert.ToDouble(txbx_pole1.Text);
                }
                else
                {
                    b = Convert.ToDouble(txbx_pole1.Text);
                }
                this.txbx_pole1.Text = "";
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_multiplication_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = this.txbx_pole1.Text + "*";
            try
            {
                znak = "*";
                if (a == 0)
                {
                    a = Convert.ToDouble(txbx_pole1.Text);
                }
                else
                {
                    b = Convert.ToDouble(txbx_pole1.Text);
                }
                this.txbx_pole1.Text = "";
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_Delit_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = this.txbx_pole1.Text + "/";
            try
            {
                znak = "/";
                if (a == 0)
                {
                    a = Convert.ToDouble(txbx_pole1.Text);
                }
                else
                {
                    b = Convert.ToDouble(txbx_pole1.Text);
                }
                this.txbx_pole1.Text = "";
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "sin(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Sin(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Sin(Convert.ToDouble(txbx_pole1.Text))).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "sin(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Sin(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            txbx_history.Text = null;
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {

                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "cos(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Cos(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Cos(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "cos(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Cos(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            txbx_history.Text = null;
        }

        private void btn_tan_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "tan(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Tan(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Tan(Convert.ToDouble(txbx_pole1.Text))).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "tan(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Tan(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            txbx_history.Text = null;
        }

        private void btn_exponentiation_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = "";
            txbx_history.Text = this.txbx_pole1.Text + "^";
            try
            {
                znak = "^";
                if (a == 0)
                {
                    a = Convert.ToDouble(txbx_pole1.Text);
                }
                else
                {
                    b = Convert.ToDouble(txbx_pole1.Text);
                }
                this.txbx_pole1.Text = "";
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        double ConvertToGradus(ref double znach)
        {
            return znach / 180 * Math.PI;
        }


        private void btn_factor_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = "!" + this.txbx_pole1.Text;
            try
            {
                double fac = 1;
                for (int i = Convert.ToInt32(txbx_pole1.Text); i > 0; i--)
                {
                    fac = fac * i;
                }
              txbx_pole1.Text = fac.ToString();
              richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
            }
            catch
            {
                MessageBox.Show("Поле не может быть пустым");
            }
        }

        private void btn_exp_Click(object sender, EventArgs e)
        {
                this.txbx_history.Text = "exp(" + this.txbx_pole1.Text + ")";
            try
            {
                a = Math.Exp(Convert.ToDouble(txbx_pole1.Text));
                richTextBox1.AppendText("exp(" + txbx_pole1.Text + ")" + "="  + Convert.ToString(a) + "\n");
                txbx_pole1.Text = a.ToString();
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_sgrt_Click(object sender, EventArgs e)
        {

            this.txbx_history.Text = "√" + this.txbx_pole1.Text;
            try
            {
                a = (Math.Sqrt(Convert.ToDouble(txbx_pole1.Text)));
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
            richTextBox1.AppendText ("√" + txbx_pole1.Text +"="+ Convert.ToString(a)+"\n");
        }

          private void btn_procent_Click(object sender, EventArgs e)
          {
              this.txbx_history.Text = this.txbx_pole1.Text + "%";
              try
              {
                  txbx_pole1.Text = (Convert.ToDouble(txbx_pole1.Text) / 100 * Convert.ToDouble(txbx_pole1.Text)).ToString();
              }
              catch
              {
                  MessageBox.Show("Не верные данные");
              }
          }

        private void btn_log_Click(object sender, EventArgs e)
        {
            this.txbx_history.Text = "log(" + this.txbx_pole1.Text + ")";
            try
            {
                a = Math.Log10(Convert.ToDouble(txbx_pole1.Text));
                richTextBox1.AppendText("Log(" + txbx_pole1.Text + ")" + "=" + Convert.ToString(a) + "\n");
            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }
        }

        private void btn_delet_Click(object sender, EventArgs e)
        {
            txbx_pole1.Text = null;
            a = 0;
            b = 0;
        }

        private void txbx_pole1_TextChanged(object sender, EventArgs e)
        {
            string S;
            string tmp = txbx_pole1.Text.Trim();
            S = string.Empty;
            bool zapyataya = true;
        
            if (tmp.Length > 1 && tmp[0] == '0' && tmp[1] != ',')
            {
                tmp = tmp.Substring(1);
            }
            
                tmp = txbx_pole1.Text + "";
            foreach (char ch in tmp)
            if (Char.IsDigit(ch) || (ch == ',' && zapyataya))
                {
                    S += ch;
                    if (ch == ',')
                    zapyataya = false; 
            }
            txbx_pole1.Text = S;
            txbx_pole1.SelectionStart = S.Length;
        }
     
        private void btn_rez_Click(object sender, EventArgs e)
        {
            txbx_history.Text = null;
            try
            {
                b = Convert.ToDouble(this.txbx_pole1.Text);
                switch (znak)
                {
                    case "+":
                        {
                            rezultat = a + b;
                            richTextBox1.AppendText(Convert.ToString(a) + "+" + Convert.ToString(b) + "=" + Convert.ToString(rezultat) + "\n");
                            break;
                        }
                    case "-":
                        {
                            rezultat = a - b;
                            richTextBox1.AppendText(Convert.ToString(a) + "-" + Convert.ToString(b) + "=" + Convert.ToString(rezultat) + "\n");
                            break;
                        }
                    case "*":
                        {
                            rezultat = a * b;
                            richTextBox1.AppendText(Convert.ToString(a) + "*" + Convert.ToString(b) + "=" + Convert.ToString(rezultat) + "\n");
                            break;
                        }
                    case "/":
                        {
                            rezultat = a / b;
                            richTextBox1.AppendText(Convert.ToString(a) + "/" + Convert.ToString(b) + "=" + Convert.ToString(rezultat) + "\n");
                            break;
                        }
                    case "^":
                        {
                            rezultat = Math.Pow(a, b);
                            richTextBox1.AppendText(Convert.ToString(a) + "^" + Convert.ToString(b) + "=" + Convert.ToString(rezultat) + "\n");
                            break;
                        }
                }
                txbx_pole1.Text = rezultat.ToString();
                a = rezultat;
                znak = "";
            }
            catch
            {
                MessageBox.Show("Неверные данные");
            }
        }

        private void btn_drob_Click_1(object sender, EventArgs e)
        {
            this.txbx_history.Text = "1/" + this.txbx_pole1.Text;
            try
            {
                txbx_pole1.Text = Convert.ToString(1 / Convert.ToDouble(txbx_pole1.Text));

            }
            catch
            {
                MessageBox.Show("Не верные данные");
            }

        }

        private void btn_tanh_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "tanh(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Cos(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Tanh(Convert.ToDouble(txbx_pole1.Text))).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "tanh(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Tanh(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
        }

        private void btn_cosh_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "cosh(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Cosh(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Cosh(Convert.ToDouble(txbx_pole1.Text))).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "cosh(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Cosh(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
        }

        private void btn_sinh_Click(object sender, EventArgs e)
        {
            if (rbtn_Radian.Checked == true)
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "sinh(" + this.txbx_pole1.Text + "rad" + ")";
                    txbx_pole1.Text = (Math.Sinh(Convert.ToDouble(txbx_pole1.Text))).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else if (txbx_pole1.Text != "")
                    txbx_pole1.Text = (Math.Sinh(Convert.ToDouble(txbx_pole1.Text))).ToString();
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            else
            {
                if (txbx_pole1.Text != "")
                {
                    this.txbx_history.Text = "sinh(" + this.txbx_pole1.Text + "°" + ")";
                    txbx_pole1.Text = (Math.Sinh(Convert.ToDouble(txbx_pole1.Text) / 180 * Math.PI)).ToString();
                    richTextBox1.AppendText(txbx_history.Text + "=" + txbx_pole1.Text + "\n");
                }
                else
                    MessageBox.Show("Поле не может быть пустым");
            }
            txbx_history.Text = null;
        }

        private void btn_comma_Click(object sender, EventArgs e)
        {
            if (txbx_pole1.Text == "")
            this.txbx_pole1.Text = txbx_pole1.Text + "0,";
            if (!txbx_pole1.Text.Contains(","))
            this.txbx_pole1.Text = txbx_pole1.Text + ",";
        }

        private void btn_delet_symbol_Click(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text.Substring(0, txbx_pole1.Text.Length - 1);
            if (txbx_pole1.Text == "")
                txbx_pole1.Text = "0";
        }

        private void txbx_history_TextChanged(object sender, EventArgs e)
        {

        }
        private void btn_1_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "1";

        }

        private void btn_2_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "2";
        }

        
        private void btn_3_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "3";
        }

        private void btn_4_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "4";
        }

        private void btn_5_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "5";
        }

        private void btn_6_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "6";
        }

        private void btn_7_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "7";
        }

        private void btn_8_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "8";
        }

        private void btn_9_Click_1(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "9";
        }
        private void btn_0_Click(object sender, EventArgs e)
        {
            txbx_pole1.Text = txbx_pole1.Text + "0";
        }

    }
}




