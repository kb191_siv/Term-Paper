﻿namespace Calculator
{
    partial class Calculator
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculator));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.txbx_pole1 = new System.Windows.Forms.TextBox();
            this.btn_Plus = new System.Windows.Forms.Button();
            this.btn_Minus = new System.Windows.Forms.Button();
            this.btn_multiplication = new System.Windows.Forms.Button();
            this.btn_Delit = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.rbtn_Gradus = new System.Windows.Forms.RadioButton();
            this.rbtn_Radian = new System.Windows.Forms.RadioButton();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_tan = new System.Windows.Forms.Button();
            this.btn_exponentiation = new System.Windows.Forms.Button();
            this.btn_factor = new System.Windows.Forms.Button();
            this.btn_exp = new System.Windows.Forms.Button();
            this.btn_sgrt = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.btn_delet = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_0 = new System.Windows.Forms.Button();
            this.btn_comma = new System.Windows.Forms.Button();
            this.btn_rez = new System.Windows.Forms.Button();
            this.txbx_history = new System.Windows.Forms.TextBox();
            this.btn_drob = new System.Windows.Forms.Button();
            this.btn_tanh = new System.Windows.Forms.Button();
            this.btn_cosh = new System.Windows.Forms.Button();
            this.btn_sinh = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btn_delet_symbol = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.contextMenuStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // txbx_pole1
            // 
            this.txbx_pole1.BackColor = System.Drawing.SystemColors.Control;
            this.txbx_pole1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbx_pole1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbx_pole1.Location = new System.Drawing.Point(12, 78);
            this.txbx_pole1.Multiline = true;
            this.txbx_pole1.Name = "txbx_pole1";
            this.txbx_pole1.Size = new System.Drawing.Size(516, 118);
            this.txbx_pole1.TabIndex = 2;
            this.txbx_pole1.TextChanged += new System.EventHandler(this.txbx_pole1_TextChanged);
            // 
            // btn_Plus
            // 
            this.btn_Plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Plus.Location = new System.Drawing.Point(469, 202);
            this.btn_Plus.Name = "btn_Plus";
            this.btn_Plus.Size = new System.Drawing.Size(59, 43);
            this.btn_Plus.TabIndex = 3;
            this.btn_Plus.Text = "+";
            this.btn_Plus.UseVisualStyleBackColor = true;
            this.btn_Plus.Click += new System.EventHandler(this.btn_Plus_Click);
            // 
            // btn_Minus
            // 
            this.btn_Minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Minus.Location = new System.Drawing.Point(469, 251);
            this.btn_Minus.Name = "btn_Minus";
            this.btn_Minus.Size = new System.Drawing.Size(59, 43);
            this.btn_Minus.TabIndex = 4;
            this.btn_Minus.Text = "-";
            this.btn_Minus.UseVisualStyleBackColor = true;
            this.btn_Minus.Click += new System.EventHandler(this.btn_Minus_Click);
            // 
            // btn_multiplication
            // 
            this.btn_multiplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_multiplication.Location = new System.Drawing.Point(469, 300);
            this.btn_multiplication.Name = "btn_multiplication";
            this.btn_multiplication.Size = new System.Drawing.Size(59, 43);
            this.btn_multiplication.TabIndex = 5;
            this.btn_multiplication.Text = "*";
            this.btn_multiplication.UseVisualStyleBackColor = true;
            this.btn_multiplication.Click += new System.EventHandler(this.btn_multiplication_Click);
            // 
            // btn_Delit
            // 
            this.btn_Delit.Location = new System.Drawing.Point(399, 202);
            this.btn_Delit.Name = "btn_Delit";
            this.btn_Delit.Size = new System.Drawing.Size(60, 43);
            this.btn_Delit.TabIndex = 6;
            this.btn_Delit.Text = "/";
            this.btn_Delit.UseVisualStyleBackColor = true;
            this.btn_Delit.Click += new System.EventHandler(this.btn_Delit_Click);
            // 
            // btn_sin
            // 
            this.btn_sin.Location = new System.Drawing.Point(12, 251);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(68, 43);
            this.btn_sin.TabIndex = 7;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click);
            // 
            // rbtn_Gradus
            // 
            this.rbtn_Gradus.AutoSize = true;
            this.rbtn_Gradus.Location = new System.Drawing.Point(12, 215);
            this.rbtn_Gradus.Name = "rbtn_Gradus";
            this.rbtn_Gradus.Size = new System.Drawing.Size(68, 17);
            this.rbtn_Gradus.TabIndex = 8;
            this.rbtn_Gradus.Text = "Градусы";
            this.rbtn_Gradus.UseVisualStyleBackColor = true;
            // 
            // rbtn_Radian
            // 
            this.rbtn_Radian.AutoSize = true;
            this.rbtn_Radian.Checked = true;
            this.rbtn_Radian.Location = new System.Drawing.Point(86, 215);
            this.rbtn_Radian.Name = "rbtn_Radian";
            this.rbtn_Radian.Size = new System.Drawing.Size(70, 17);
            this.rbtn_Radian.TabIndex = 9;
            this.rbtn_Radian.TabStop = true;
            this.rbtn_Radian.Text = "Радианы";
            this.rbtn_Radian.UseVisualStyleBackColor = true;
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(86, 251);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(70, 43);
            this.btn_cos.TabIndex = 10;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
            // 
            // btn_tan
            // 
            this.btn_tan.Location = new System.Drawing.Point(88, 398);
            this.btn_tan.Name = "btn_tan";
            this.btn_tan.Size = new System.Drawing.Size(68, 43);
            this.btn_tan.TabIndex = 11;
            this.btn_tan.Text = "tan";
            this.btn_tan.UseVisualStyleBackColor = true;
            this.btn_tan.Click += new System.EventHandler(this.btn_tan_Click);
            // 
            // btn_exponentiation
            // 
            this.btn_exponentiation.Location = new System.Drawing.Point(12, 300);
            this.btn_exponentiation.Name = "btn_exponentiation";
            this.btn_exponentiation.Size = new System.Drawing.Size(70, 43);
            this.btn_exponentiation.TabIndex = 12;
            this.btn_exponentiation.Text = "x^y";
            this.btn_exponentiation.UseVisualStyleBackColor = true;
            this.btn_exponentiation.Click += new System.EventHandler(this.btn_exponentiation_Click);
            // 
            // btn_factor
            // 
            this.btn_factor.Location = new System.Drawing.Point(86, 300);
            this.btn_factor.Name = "btn_factor";
            this.btn_factor.Size = new System.Drawing.Size(70, 43);
            this.btn_factor.TabIndex = 13;
            this.btn_factor.Text = "n!";
            this.btn_factor.UseVisualStyleBackColor = true;
            this.btn_factor.Click += new System.EventHandler(this.btn_factor_Click);
            // 
            // btn_exp
            // 
            this.btn_exp.Location = new System.Drawing.Point(12, 398);
            this.btn_exp.Name = "btn_exp";
            this.btn_exp.Size = new System.Drawing.Size(68, 43);
            this.btn_exp.TabIndex = 14;
            this.btn_exp.Text = "exp(x)";
            this.btn_exp.UseVisualStyleBackColor = true;
            this.btn_exp.Click += new System.EventHandler(this.btn_exp_Click);
            // 
            // btn_sgrt
            // 
            this.btn_sgrt.Location = new System.Drawing.Point(333, 202);
            this.btn_sgrt.Name = "btn_sgrt";
            this.btn_sgrt.Size = new System.Drawing.Size(60, 43);
            this.btn_sgrt.TabIndex = 15;
            this.btn_sgrt.Text = "√";
            this.btn_sgrt.UseVisualStyleBackColor = true;
            this.btn_sgrt.Click += new System.EventHandler(this.btn_sgrt_Click);
            // 
            // btn_log
            // 
            this.btn_log.Location = new System.Drawing.Point(12, 349);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(68, 43);
            this.btn_log.TabIndex = 17;
            this.btn_log.Text = "log";
            this.btn_log.UseVisualStyleBackColor = true;
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
            // 
            // btn_delet
            // 
            this.btn_delet.Location = new System.Drawing.Point(162, 251);
            this.btn_delet.Name = "btn_delet";
            this.btn_delet.Size = new System.Drawing.Size(68, 43);
            this.btn_delet.TabIndex = 18;
            this.btn_delet.Text = "с";
            this.btn_delet.UseVisualStyleBackColor = true;
            this.btn_delet.Click += new System.EventHandler(this.btn_delet_Click);
            // 
            // btn_7
            // 
            this.btn_7.Location = new System.Drawing.Point(267, 251);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(60, 43);
            this.btn_7.TabIndex = 19;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = true;
            this.btn_7.Click += new System.EventHandler(this.btn_7_Click_1);
            // 
            // btn_8
            // 
            this.btn_8.Location = new System.Drawing.Point(333, 251);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(60, 43);
            this.btn_8.TabIndex = 20;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = true;
            this.btn_8.Click += new System.EventHandler(this.btn_8_Click_1);
            // 
            // btn_9
            // 
            this.btn_9.Location = new System.Drawing.Point(399, 251);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(60, 43);
            this.btn_9.TabIndex = 21;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = true;
            this.btn_9.Click += new System.EventHandler(this.btn_9_Click_1);
            // 
            // btn_4
            // 
            this.btn_4.Location = new System.Drawing.Point(267, 300);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(60, 43);
            this.btn_4.TabIndex = 22;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = true;
            this.btn_4.Click += new System.EventHandler(this.btn_4_Click_1);
            // 
            // btn_5
            // 
            this.btn_5.Location = new System.Drawing.Point(333, 300);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(60, 43);
            this.btn_5.TabIndex = 23;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = true;
            this.btn_5.Click += new System.EventHandler(this.btn_5_Click_1);
            // 
            // btn_6
            // 
            this.btn_6.Location = new System.Drawing.Point(399, 300);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(60, 43);
            this.btn_6.TabIndex = 24;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = true;
            this.btn_6.Click += new System.EventHandler(this.btn_6_Click_1);
            // 
            // btn_1
            // 
            this.btn_1.Location = new System.Drawing.Point(267, 349);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(60, 43);
            this.btn_1.TabIndex = 25;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = true;
            this.btn_1.Click += new System.EventHandler(this.btn_1_Click_1);
            // 
            // btn_2
            // 
            this.btn_2.Location = new System.Drawing.Point(333, 349);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(60, 43);
            this.btn_2.TabIndex = 26;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = true;
            this.btn_2.Click += new System.EventHandler(this.btn_2_Click_1);
            // 
            // btn_3
            // 
            this.btn_3.Location = new System.Drawing.Point(399, 349);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(60, 43);
            this.btn_3.TabIndex = 27;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = true;
            this.btn_3.Click += new System.EventHandler(this.btn_3_Click_1);
            // 
            // btn_0
            // 
            this.btn_0.Location = new System.Drawing.Point(267, 398);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(126, 43);
            this.btn_0.TabIndex = 28;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = true;
            this.btn_0.Click += new System.EventHandler(this.btn_0_Click);
            // 
            // btn_comma
            // 
            this.btn_comma.Location = new System.Drawing.Point(399, 398);
            this.btn_comma.Name = "btn_comma";
            this.btn_comma.Size = new System.Drawing.Size(60, 43);
            this.btn_comma.TabIndex = 29;
            this.btn_comma.Text = ",";
            this.btn_comma.UseVisualStyleBackColor = true;
            this.btn_comma.Click += new System.EventHandler(this.btn_comma_Click);
            // 
            // btn_rez
            // 
            this.btn_rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_rez.Location = new System.Drawing.Point(469, 349);
            this.btn_rez.Name = "btn_rez";
            this.btn_rez.Size = new System.Drawing.Size(59, 92);
            this.btn_rez.TabIndex = 30;
            this.btn_rez.Text = "=";
            this.btn_rez.UseVisualStyleBackColor = true;
            this.btn_rez.Click += new System.EventHandler(this.btn_rez_Click);
            // 
            // txbx_history
            // 
            this.txbx_history.BackColor = System.Drawing.SystemColors.Control;
            this.txbx_history.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbx_history.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txbx_history.Location = new System.Drawing.Point(223, 23);
            this.txbx_history.Name = "txbx_history";
            this.txbx_history.ReadOnly = true;
            this.txbx_history.Size = new System.Drawing.Size(305, 31);
            this.txbx_history.TabIndex = 31;
            this.txbx_history.TextChanged += new System.EventHandler(this.txbx_history_TextChanged);
            // 
            // btn_drob
            // 
            this.btn_drob.Location = new System.Drawing.Point(162, 300);
            this.btn_drob.Name = "btn_drob";
            this.btn_drob.Size = new System.Drawing.Size(68, 43);
            this.btn_drob.TabIndex = 32;
            this.btn_drob.Text = "1/x";
            this.btn_drob.UseVisualStyleBackColor = true;
            this.btn_drob.Click += new System.EventHandler(this.btn_drob_Click_1);
            // 
            // btn_tanh
            // 
            this.btn_tanh.Location = new System.Drawing.Point(162, 349);
            this.btn_tanh.Name = "btn_tanh";
            this.btn_tanh.Size = new System.Drawing.Size(68, 43);
            this.btn_tanh.TabIndex = 33;
            this.btn_tanh.Text = "tanh";
            this.btn_tanh.UseVisualStyleBackColor = true;
            this.btn_tanh.Click += new System.EventHandler(this.btn_tanh_Click);
            // 
            // btn_cosh
            // 
            this.btn_cosh.Location = new System.Drawing.Point(88, 349);
            this.btn_cosh.Name = "btn_cosh";
            this.btn_cosh.Size = new System.Drawing.Size(68, 43);
            this.btn_cosh.TabIndex = 34;
            this.btn_cosh.Text = "cosh";
            this.btn_cosh.UseVisualStyleBackColor = true;
            this.btn_cosh.Click += new System.EventHandler(this.btn_cosh_Click);
            // 
            // btn_sinh
            // 
            this.btn_sinh.Location = new System.Drawing.Point(162, 399);
            this.btn_sinh.Name = "btn_sinh";
            this.btn_sinh.Size = new System.Drawing.Size(68, 43);
            this.btn_sinh.TabIndex = 35;
            this.btn_sinh.Text = "sinh";
            this.btn_sinh.UseVisualStyleBackColor = true;
            this.btn_sinh.Click += new System.EventHandler(this.btn_sinh_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.richTextBox1.Location = new System.Drawing.Point(534, 57);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(271, 385);
            this.richTextBox1.TabIndex = 36;
            this.richTextBox1.Text = "";
            // 
            // btn_delet_symbol
            // 
            this.btn_delet_symbol.Location = new System.Drawing.Point(267, 202);
            this.btn_delet_symbol.Name = "btn_delet_symbol";
            this.btn_delet_symbol.Size = new System.Drawing.Size(60, 43);
            this.btn_delet_symbol.TabIndex = 37;
            this.btn_delet_symbol.Text = "⌫";
            this.btn_delet_symbol.UseVisualStyleBackColor = true;
            this.btn_delet_symbol.Click += new System.EventHandler(this.btn_delet_symbol_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(677, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 33);
            this.label1.TabIndex = 38;
            this.label1.Text = "История";
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(182, 31);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 492);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_delet_symbol);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btn_sinh);
            this.Controls.Add(this.btn_cosh);
            this.Controls.Add(this.btn_tanh);
            this.Controls.Add(this.btn_drob);
            this.Controls.Add(this.txbx_history);
            this.Controls.Add(this.btn_rez);
            this.Controls.Add(this.btn_comma);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_delet);
            this.Controls.Add(this.btn_log);
            this.Controls.Add(this.btn_sgrt);
            this.Controls.Add(this.btn_exp);
            this.Controls.Add(this.btn_factor);
            this.Controls.Add(this.btn_exponentiation);
            this.Controls.Add(this.btn_tan);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.rbtn_Radian);
            this.Controls.Add(this.rbtn_Gradus);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_Delit);
            this.Controls.Add(this.btn_multiplication);
            this.Controls.Add(this.btn_Minus);
            this.Controls.Add(this.btn_Plus);
            this.Controls.Add(this.txbx_pole1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(826, 531);
            this.Name = "Calculator";
            this.Text = "Калькулятор";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.TextBox txbx_pole1;
        private System.Windows.Forms.Button btn_Plus;
        private System.Windows.Forms.Button btn_Minus;
        private System.Windows.Forms.Button btn_multiplication;
        private System.Windows.Forms.Button btn_Delit;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.RadioButton rbtn_Gradus;
        private System.Windows.Forms.RadioButton rbtn_Radian;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_tan;
        private System.Windows.Forms.Button btn_exponentiation;
        private System.Windows.Forms.Button btn_factor;
        private System.Windows.Forms.Button btn_exp;
        private System.Windows.Forms.Button btn_sgrt;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_delet;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button btn_comma;
        private System.Windows.Forms.Button btn_rez;
        private System.Windows.Forms.TextBox txbx_history;
        private System.Windows.Forms.Button btn_drob;
        private System.Windows.Forms.Button btn_tanh;
        private System.Windows.Forms.Button btn_cosh;
        private System.Windows.Forms.Button btn_sinh;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_delet_symbol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
    }
}

